import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class LiteratureStudent extends Student {

	static List<LiteratureStudent> literatureAttendanceSheetList = new ArrayList<LiteratureStudent>();

	public LiteratureStudent(String faculty, String name, String fatherName,
			String department) {
		super(faculty, name, fatherName, department);

	}

	@Override
	public void addToDepartment() {

		StudentTypeIdentifier.addStudentToDepartmentList(this);

	}

	@Override
	public void createEmail() {
		StringTokenizer st = new StringTokenizer(this.name, " ");
		this.name = "";
		while (st.hasMoreTokens()) {
			this.name = this.name + st.nextToken();
		}
		this.email = this.name + "@literature.hu.edu";
	}

	@Override
	public void createCreditSheetForm() {
		this.creditSheet = "Credit sheet form for " + this.name + ":\n"
				+ "Subject\t\t\t Number credits\n" + "Dari\t\t\t 5\n"
				+ "Pashto\t\t\t 5\n" + "Safat\t\t\t 1\n" + "Arabic\t\t\t 5\n"
				+ "Poem\t\t\t 5";
	}

	@Override
	public void addToAttendanceSheet() {
		literatureAttendanceSheetList.add(this);

	}

	@Override
	public void attendanceSheetDisplay() {
		int number = 0;
		System.out.println("Number\t\tName\t\tFather Name\t\tDepartment");
		for (Student current : literatureAttendanceSheetList) {
			
			System.out.println(++number + "\t\t" + current.name + "\t\t"
					+ current.fatherName+"\t\t"+current.department);

		}
	}
}
