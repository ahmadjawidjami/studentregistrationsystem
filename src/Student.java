import java.util.ArrayList;
import java.util.List;


public abstract class Student {
	public String faculty;
    public String name;
    public String fatherName;
    public String department;
    public String email;
    
    public String creditSheet;


    public Student(String faculty, String name, String fatherName, String department) {
    	this.faculty = faculty;
        this.name = name;
        this.fatherName = fatherName;
        this.department = department;

    }


    public abstract void addToDepartment();

    public abstract void createEmail();

    public abstract void createCreditSheetForm();

    public abstract void addToAttendanceSheet();
    
    public abstract void attendanceSheetDisplay();


}









