import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created with IntelliJ IDEA. User: Ahmad Jawid Date: 9/6/13 Time: 9:43 PM To
 * change this template use File | Settings | File Templates.
 */
public class EngineeringStudent extends Student {

	static List<EngineeringStudent> engineeringAttendanceSheetList = new ArrayList<EngineeringStudent>();

	public EngineeringStudent(String faculty, String name, String fatherName,
			String department) {
		super(faculty, name, fatherName, department);

	}

	@Override
	public void addToDepartment() {
		StudentTypeIdentifier.addStudentToDepartmentList(this);
	}

	@Override
	public void createEmail() {

		StringTokenizer st = new StringTokenizer(this.name, " ");
		this.name = "";
		while (st.hasMoreTokens()) {
			this.name = this.name + st.nextToken();
		}
		this.email = this.name + "@engineering.hu.edu";
	}

	@Override
	public void createCreditSheetForm() {
		this.creditSheet = "Credit sheet form for " + this.name + ":\n"
				+ "Subject\t\t\t Number credits\n" + "Mathematics\t\t\t 4\n"
				+ "Geometrics\t\t\t 3\n" + "Safat\t\t\t\t 1\n"
				+ "Physics\t\t\t\t 4\n" + "Drawing\t\t\t\t 5";
	}

	@Override
	public void addToAttendanceSheet() {
		engineeringAttendanceSheetList.add(this);
	}

	@Override
	public void attendanceSheetDisplay() {
		int number = 0;
		System.out.println("Number\t\tName\t\tFather Name\t\tDepartment");
		for (Student current : engineeringAttendanceSheetList) {
			System.out.println(++number + "\t\t" + current.name + "\t\t"
					+ current.fatherName+"\t\t"+current.department);

		}
	}
}
