
public class StudentRegisterer {

    StudentTypeIdentifier studentIdentifier;

    public StudentRegisterer(StudentTypeIdentifier studentIdentifier){
        this.studentIdentifier = studentIdentifier;
    }

    public Student registerStudent(String faculty, String name, String fatherName, String department){

        Student student;
        student = studentIdentifier.createStudent(faculty, name, fatherName, department);
        
        student.createEmail();
        student.createCreditSheetForm();
        student.addToDepartment();
        student.addToAttendanceSheet();
        return student;

    }

}
