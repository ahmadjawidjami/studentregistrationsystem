import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA. User: Ahmad Jawid Date: 9/6/13 Time: 9:11 PM To
 * change this template use File | Settings | File Templates.
 */
public class StudentTypeIdentifier {
	
	
	public static List <String> listOfFaculties = new ArrayList <String>();
	public static List<String> departmentsOfAFaculty = new ArrayList<String>();

	String computerScience;
	String engineering;
	String literature;

	public static List<Student> softwareEngineeringStudents = new ArrayList<Student>();
	public static List<Student> databaseStudents = new ArrayList<Student>();
	public static List<Student> NetWorkStudents = new ArrayList<Student>();

	public static List<Student> civilStudent = new ArrayList<Student>();
	public static List<Student> architectureStuddents = new ArrayList<Student>();
	public static List<Student> electronicsStudent = new ArrayList<Student>();

	public static List<Student> dariStudents = new ArrayList<Student>();
	public static List<Student> pashtoStudents = new ArrayList<Student>();
	public static List<Student> arabicStudents = new ArrayList<Student>();

	public StudentTypeIdentifier() {
		computerScience = "Computer Science";
		listOfFaculties.add(computerScience);
		engineering = "Engineering";
		listOfFaculties.add(engineering);
		literature = "Literaute";
		listOfFaculties.add(literature);

	}

	public static void addStudentToDepartmentList(Student student) {

		if (student.department.equalsIgnoreCase("Software Engineering")) {
			softwareEngineeringStudents.add(student);
		} else if (student.department.equalsIgnoreCase("Database")) {
			databaseStudents.add(student);
		} else if (student.department.equalsIgnoreCase("Network")) {
			NetWorkStudents.add(student);
		} else if (student.department.equalsIgnoreCase("Civil")) {
			civilStudent.add(student);
		} else if (student.department.equalsIgnoreCase("Architecture")) {
			architectureStuddents.add(student);
		} else if (student.department.equalsIgnoreCase("Electronics")) {
			electronicsStudent.add(student);
		}

		else if (student.department.equalsIgnoreCase("Dari")) {
			dariStudents.add(student);
		} else if (student.department.equalsIgnoreCase("Pashto")) {
			pashtoStudents.add(student);
		} else if (student.department.equalsIgnoreCase("Arabic")) {
			arabicStudents.add(student);
		}

	}

	public void specifyDepartments(String faculty) {

		if (faculty.equalsIgnoreCase(computerScience)) {
			departmentsOfAFaculty.add("Software Engineering");
			departmentsOfAFaculty.add("Database");
			departmentsOfAFaculty.add("Network");

		} else if (faculty.equalsIgnoreCase(engineering)) {
			departmentsOfAFaculty.add("Civil");
			departmentsOfAFaculty.add("Architecture");
			departmentsOfAFaculty.add("Electronics");

		} else if (faculty.equalsIgnoreCase(literature)) {
			departmentsOfAFaculty.add("Dari");
			departmentsOfAFaculty.add("Pashto");
			departmentsOfAFaculty.add("Arabic");
		}

	}

	public Student createStudent(String faculty, String name,
			String fatherName, String department) {

		listOfFaculties.clear();

		departmentsOfAFaculty.clear();

		Student student = null;

		if (faculty.equalsIgnoreCase(computerScience)) {
			student = new ComputerScienceStudent(faculty, name, fatherName,
					department);

		} else if (faculty.equalsIgnoreCase(engineering)) {
			student = new EngineeringStudent(faculty, name, fatherName,
					department);

		} else if (faculty.equalsIgnoreCase(literature)) {
			student = new LiteratureStudent(faculty, name, fatherName,
					department);

		}

		return student;

	}

}
