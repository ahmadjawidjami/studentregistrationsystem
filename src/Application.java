import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JComboBox;
import javax.swing.JButton;

public class Application extends JFrame {

	private JPanel contentPane;
	private JTextField txtName;
	private JTextField txtFatherName;

	JComboBox facultyComboBox;
	JComboBox departmentComboBox;
	static StudentTypeIdentifier studentIdentifier;
	static StudentRegisterer registerer;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		studentIdentifier = new StudentTypeIdentifier();
		registerer = new StudentRegisterer(studentIdentifier);

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Application frame = new Application();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Application() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);

		txtName = new JTextField();
		txtName.setText("Name");
		txtName.setColumns(10);

		JLabel lblName = new JLabel("Name");

		txtFatherName = new JTextField();
		txtFatherName.setColumns(10);

		JLabel lblFatherName = new JLabel("Father Name");

		facultyComboBox = new JComboBox();
		JLabel lblFaculty = new JLabel("Faculty");
		departmentComboBox = new JComboBox();

		JLabel lblDepartment = new JLabel("Department");

		facultyComboBox.addItem("");
		for (String currentFaculty : StudentTypeIdentifier.listOfFaculties) {
			facultyComboBox.addItem(currentFaculty);
		}

		facultyComboBox.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				departmentComboBox.removeAllItems();
				studentIdentifier.specifyDepartments((String) facultyComboBox
						.getSelectedItem());

				for (String currentDepartment : StudentTypeIdentifier.departmentsOfAFaculty) {
					departmentComboBox.addItem(currentDepartment);

				}

				StudentTypeIdentifier.departmentsOfAFaculty.clear();

			}
		});

		JButton btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!txtName.getText().equals("")
						&& !txtFatherName.getText().equals("")
						&& (String) facultyComboBox.getSelectedItem() != null
						&& (String) departmentComboBox.getSelectedItem() != null) {

					Student studentForDisplay = registerer.registerStudent(
							(String) facultyComboBox.getSelectedItem(),
							txtName.getText(), txtFatherName.getText(),
							(String) departmentComboBox.getSelectedItem());
					System.out.println(studentForDisplay.name + "\t"
							+ studentForDisplay.fatherName + "\t"
							+ studentForDisplay.faculty + "\t"
							+ studentForDisplay.department + "\t"
							+ studentForDisplay.email);
					System.out.println("\n"+studentForDisplay.creditSheet);
					
					System.out.println("\nAttendance sheet list for "+studentForDisplay.faculty+" faculy:");
					studentForDisplay.attendanceSheetDisplay();
					

				}

			}
		});
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(gl_panel
				.createParallelGroup(Alignment.LEADING)
				.addGroup(
						gl_panel.createSequentialGroup()
								.addGroup(
										gl_panel.createParallelGroup(
												Alignment.LEADING)
												.addGroup(
														gl_panel.createSequentialGroup()
																.addGap(50)
																.addComponent(
																		lblName)
																.addGap(124)
																.addComponent(
																		lblFatherName)
																.addGap(65)
																.addComponent(
																		lblFaculty)
																.addGap(161)
																.addComponent(
																		lblDepartment))
												.addGroup(
														gl_panel.createSequentialGroup()
																.addGap(25)
																.addGroup(
																		gl_panel.createParallelGroup(
																				Alignment.LEADING)
																				.addComponent(
																						btnSubmit)
																				.addGroup(
																						gl_panel.createSequentialGroup()
																								.addComponent(
																										txtName,
																										GroupLayout.PREFERRED_SIZE,
																										GroupLayout.DEFAULT_SIZE,
																										GroupLayout.PREFERRED_SIZE)
																								.addGap(18)
																								.addComponent(
																										txtFatherName,
																										GroupLayout.PREFERRED_SIZE,
																										GroupLayout.DEFAULT_SIZE,
																										GroupLayout.PREFERRED_SIZE)
																								.addGap(18)
																								.addComponent(
																										facultyComboBox,
																										GroupLayout.PREFERRED_SIZE,
																										162,
																										GroupLayout.PREFERRED_SIZE)
																								.addGap(38)
																								.addComponent(
																										departmentComboBox,
																										GroupLayout.PREFERRED_SIZE,
																										153,
																										GroupLayout.PREFERRED_SIZE)))))
								.addContainerGap(108, Short.MAX_VALUE)));
		gl_panel.setVerticalGroup(gl_panel
				.createParallelGroup(Alignment.LEADING)
				.addGroup(
						gl_panel.createSequentialGroup()
								.addGap(68)
								.addGroup(
										gl_panel.createParallelGroup(
												Alignment.BASELINE)
												.addComponent(lblName)
												.addComponent(lblFatherName)
												.addComponent(lblFaculty)
												.addComponent(lblDepartment))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(
										gl_panel.createParallelGroup(
												Alignment.BASELINE)
												.addComponent(
														txtName,
														GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE)
												.addComponent(
														txtFatherName,
														GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE)
												.addComponent(
														facultyComboBox,
														GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE)
												.addComponent(
														departmentComboBox,
														GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE))
								.addGap(61).addComponent(btnSubmit)
								.addContainerGap(259, Short.MAX_VALUE)));
		panel.setLayout(gl_panel);
	}
}
