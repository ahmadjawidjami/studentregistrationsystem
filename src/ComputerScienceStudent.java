import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class ComputerScienceStudent extends Student {

	public ComputerScienceStudent(String faculty, String name,
			String fatherName, String department) {
		super(faculty, name, fatherName, department);

	}

	static List<ComputerScienceStudent> computerScienceAttendanceSheetList = new ArrayList<ComputerScienceStudent>();

	@Override
	public void addToDepartment() {

		StudentTypeIdentifier.addStudentToDepartmentList(this);

	}

	@Override
	public void createEmail() {
		StringTokenizer st = new StringTokenizer(this.name, " ");
		this.name = "";
		while (st.hasMoreTokens()) {
			this.name = this.name + st.nextToken();
		}

		this.email = this.name + "@cs.hu.edu";

	}

	@Override
	public void createCreditSheetForm() {

		this.creditSheet = "Credit sheet form for " + this.name + ":\n"
				+ "Subject\t\t\t Number credits\n"
				+ "Software Engineering\t\t 5\n"
				+ "Web Engineering\t\t\t 5\n"
				+ "Safat\t\t\t\t 1\n"
				+ "Network programming\t\t 5\n"
				+ "Database\t\t\t 5";

	}

	@Override
	public void addToAttendanceSheet() {

		computerScienceAttendanceSheetList.add(this);

	}

	@Override
	public void attendanceSheetDisplay() {
		int number = 0;
		System.out.println("Number\t\tName\t\tFather Name\t\tDepartment");

		for(Student current: computerScienceAttendanceSheetList){
			System.out.println(++number+"\t\t"+current.name+"\t\t"+current.fatherName+"\t\t"+current.department);
			
		}
		
	}
}
